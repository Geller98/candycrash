﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Background_tile : MonoBehaviour {
    public GameObject[] tiles;
	private static Background_tile tile;

    void Start() {
        initalize();
    }

    // Update is called once per frame
    void Update() {
    }

    void initalize() {
        int random = Random.Range(0, tiles.Length);
		this.transform.GetComponent<SpriteRenderer> ().sprite = tiles [random].transform.GetComponent<SpriteRenderer> ().sprite;
        /*GameObject tile = Instantiate(tiles[random], transform.position, Quaternion.identity);
        tile.transform.parent = this.transform;
        tile.name = this.gameObject.name;*/
    }

	void OnMouseDown(){
		if (tile == null) {
			tile = this;
		} else {
			if(checkTiles()){
				zameni ();
				tile = null;
			}else{
				tile = null;
			}
		}
	}

	void zameni(){
		Vector3 traform = this.transform.position;
		this.transform.position = tile.transform.position;
		tile.transform.position = traform;

		string name = this.name.ToString ();
		this.name = tile.name.ToString ();
		tile.name = name;
		this.SendMessageUpwards ("CheckRowsColumns",tile);
	}

	bool checkTiles(){
		if ((tile.transform.position.x + 1) == this.transform.position.x && tile.transform.position.y == this.transform.position.y)
			return true;
		if ((tile.transform.position.x - 1) == this.transform.position.x && tile.transform.position.y == this.transform.position.y)
			return true;
		if((tile.transform.position.y - 1) == this.transform.position.y && tile.transform.position.x == this.transform.position.x)
			return true;
		if ((tile.transform.position.y + 1) == this.transform.position.y && tile.transform.position.x == this.transform.position.x)
			return true;

		return false;
	}
}


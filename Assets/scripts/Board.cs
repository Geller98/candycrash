﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour {
    public int width;
    public int height;
	public Background_tile tilePrefab;
	public ParticleSystem particlePrefab;
	private Background_tile[,] tiles;

	// Use this for initialization
	void Start () {
		tiles = new Background_tile[width, height];
		StartCoroutine (setTiles());
	}

	IEnumerator setTiles() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
				Background_tile temp = Instantiate(tilePrefab,new Vector2(i, j), Quaternion.identity);

				//set parent and name of tile
				temp.transform.parent = this.transform;
				temp.name = "( " + i + ", " + j + " )";

				//animations & sound
				temp.transform.GetComponent<ParticleSystem> ().Play ();
				temp.transform.GetComponent<AudioSource> ().Play ();

				//put to array
				tiles [i, j] = temp;

				//wait for 0,5 sec
				yield return new WaitForSeconds (0.5f);
            }
        }
    }

	public void check3inRow(Background_tile tile){

	}
}
